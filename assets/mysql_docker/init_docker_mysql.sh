#!/bin/bash

echo -n "Izaberite opciju:
    1.Pokreni MySQL
    2.Ocistiti podatke?
: "
read OPTION

if [ $OPTION -eq 1 ] 
then
  sudo docker-compose up
else
  sudo docker-compose down --volumes
fi
