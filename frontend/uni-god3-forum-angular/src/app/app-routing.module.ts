import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FileComponent } from './components/file/file.component';
import { ForumUserComponent } from './components/forum-user/forum-user.component';
import { ForumComponent } from './components/forum/forum.component';
import { LoginComponent } from './components/login/login.component';
import { PostComponent } from './components/post/post.component';
import { RegisterComponent } from './components/register/register.component';
import { RegisteredUsersComponent } from './components/registered-users/registered-users.component';
import { RoleComponent } from './components/role/role.component';
import { TopicComponent } from './components/topic/topic.component';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'files', component: FileComponent, canActivate: [AuthGuard], data: { allowedRoles: ['ROLE_ADMIN', 'ROLE_USER'] } },
  { path: 'posts', component: PostComponent, canActivate: [AuthGuard], data: { allowedRoles: ['ROLE_ADMIN', 'ROLE_USER'] } },
  { path: 'topics', component: TopicComponent, canActivate: [AuthGuard], data: { allowedRoles: ['ROLE_ADMIN', 'ROLE_USER'] } },
  { path: 'forums', component: ForumComponent, canActivate: [AuthGuard], data: { allowedRoles: ['ROLE_ADMIN', 'ROLE_USER'] } },
  { path: 'forum-users', component: ForumUserComponent, canActivate: [AuthGuard], data: { allowedRoles: ['ROLE_ADMIN'] } },
  { path: 'registered-users', component: RegisteredUsersComponent, canActivate: [AuthGuard], data: { allowedRoles: ['ROLE_ADMIN'] } },
  { path: 'roles', component: RoleComponent, canActivate: [AuthGuard], data: { allowedRoles: ['ROLE_ADMIN'] } }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
