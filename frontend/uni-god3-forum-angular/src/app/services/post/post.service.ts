import { Injectable } from '@angular/core';
import { AbstractService } from 'src/app/modular/service/abstract-service';
import { Post } from 'src/app/models/post';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class PostService extends AbstractService<Post>{

  constructor(public httpClient: HttpClient) {
    super(httpClient, '/posts');
  }
}
