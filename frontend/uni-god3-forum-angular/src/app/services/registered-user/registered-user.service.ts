import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RegisteredUser } from 'src/app/models/registered-user';
import { AbstractService } from 'src/app/modular/service/abstract-service';

@Injectable({
  providedIn: 'root'
})
export class RegisteredUserService extends AbstractService<RegisteredUser> {

  constructor(public httpClient: HttpClient) {
    super(httpClient, '/registered-users');
  }
}
