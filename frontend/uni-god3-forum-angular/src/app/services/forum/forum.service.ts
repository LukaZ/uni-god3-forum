import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Forum } from 'src/app/models/forum';
import { AbstractService } from 'src/app/modular/service/abstract-service';

@Injectable({
  providedIn: 'root'
})
export class ForumService extends AbstractService<Forum>{

  constructor(public httpClient: HttpClient) {
    super(httpClient, '/forums');
  }
}
