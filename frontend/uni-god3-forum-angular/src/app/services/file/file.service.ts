import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { File } from 'src/app/models/file';
import { AbstractService } from 'src/app/modular/service/abstract-service';

@Injectable({
  providedIn: 'root'
})
export class FileService extends AbstractService<File>{

  constructor(public httpClient: HttpClient) {
    super(httpClient, '/files');
  }
}
