import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ForumUser } from 'src/app/models/forum-user';
import { AbstractService } from 'src/app/modular/service/abstract-service';

@Injectable({
  providedIn: 'root'
})
export class ForumUserService extends AbstractService<ForumUser>{

  constructor(public httpClient: HttpClient) {
    super(httpClient, '/forum-users');
  }
}
