import { Injectable } from '@angular/core';
import { AbstractService } from 'src/app/modular/service/abstract-service';
import { Role } from 'src/app/models/role';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class RoleService extends AbstractService<Role>{

  constructor(public httpClient: HttpClient) {
    super(httpClient, '/roles');
  }
}
