import { Injectable } from '@angular/core';
import { AbstractService } from 'src/app/modular/service/abstract-service';
import { Topic } from 'src/app/models/topic';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class TopicService extends AbstractService<Topic>{

  constructor(public httpClient: HttpClient) {
    super(httpClient, '/topics');
  }
}
