import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { TopicService } from 'src/app/services/topic/topic.service';


import { Validators } from '@angular/forms';

import { CreateObjectComponent } from 'src/app/modular/components/create-object/create-object.component';
import { FormBuilderInputRow } from 'src/app/modular/components/create-object/form-builder-input-row';
import { DataTableComponent } from 'src/app/modular/components/data-table/data-table.component';
import { ViewObjectComponent } from 'src/app/modular/components/view-object/view-object.component';
import { FileService } from 'src/app/services/file/file.service';
import { AbstractDBItem } from 'src/app/modular/abstract/abstract-db-item';
import { Topic } from 'src/app/models/topic';
import { AbstractModel } from 'src/app/models/abstract-model';
import { ForumService } from 'src/app/services/forum/forum.service';
import { PostService } from 'src/app/services/post/post.service';


@Component({
  selector: 'app-topic',
  templateUrl: './topic.component.html',
  styleUrls: ['./topic.component.scss']
})
export class TopicComponent extends AbstractDBItem implements OnInit {
  @ViewChild(DataTableComponent, { static: true }) dataTableComponent: DataTableComponent<Topic, TopicService>;
  @ViewChild(CreateObjectComponent, { static: true }) createObjectComponent: CreateObjectComponent<Topic>;

  // ['id', 'topicName', 'forum', 'posts'],
  constructor(public topicService: TopicService, public forumService: ForumService, public postService: PostService, public dialog: MatDialog) {
    super(
      [
        { name: "id" },
        { name: "topicName" },
        { name: "forum", externalService: forumService },
        { name: "posts", externalService: postService },
      ],
      [
        { inputType: 'text', name: 'topicName', controlsConfig: [null, Validators.required, null] },
        { inputType: 'foreign_pk', name: 'forum', controlsConfig: [null, Validators.required, null], externalService: forumService },
      ]
    )
  }


  ngOnInit(): void {
    this.refreshView();
  }

  refreshView(): void {
    this.topicService.getAll().subscribe(
      r => {
        this.dataTableComponent.dataSource = r['content'];
      },
      err => console.log(err));
  }

  onRowClicked_View(item: Topic) {
    const dialogRef = this.dialog.open(ViewObjectComponent, {
      data: item,
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed', result);
    });
  }

  onRowClicked_ViewChild(item: AbstractModel) {
    const dialogRef = this.dialog.open(ViewObjectComponent, {
      data: item,
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed', result);
    });
  }

  onRowClicked_Edit(item: Topic) {
    this.createObjectComponent.startEdit(item);
    // this.createObjectComponent.createObjectForm.patchValue(item);
  }

  onRowClicked_Delete(item: Topic) {
    console.log('Delete clicked: ', item.id);
    this.topicService.delete(item.id).subscribe(r => {
      this.refreshView();
    });
  }
}
