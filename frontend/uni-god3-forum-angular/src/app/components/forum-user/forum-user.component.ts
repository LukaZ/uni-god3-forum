import { Component, OnInit, ViewChild } from '@angular/core';
import { Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';


import { CreateObjectComponent } from 'src/app/modular/components/create-object/create-object.component';
import { FormBuilderInputRow } from 'src/app/modular/components/create-object/form-builder-input-row';
import { DataTableComponent } from 'src/app/modular/components/data-table/data-table.component';
import { ViewObjectComponent } from 'src/app/modular/components/view-object/view-object.component';

import { AbstractDBItem } from 'src/app/modular/abstract/abstract-db-item';
import { AbstractModel } from 'src/app/models/abstract-model';
import { ForumUserService } from 'src/app/services/forum-user/forum-user.service';
import { ForumUser } from 'src/app/models/forum-user';
import { ForumService } from 'src/app/services/forum/forum.service';
import { RegisteredUserService } from 'src/app/services/registered-user/registered-user.service';
import { RoleService } from 'src/app/services/role/role.service';

@Component({
  selector: 'app-forum-user',
  templateUrl: './forum-user.component.html',
  styleUrls: ['./forum-user.component.scss']
})
export class ForumUserComponent extends AbstractDBItem implements OnInit {
  @ViewChild(DataTableComponent, { static: true }) dataTableComponent: DataTableComponent<ForumUser, ForumUserService>;
  @ViewChild(CreateObjectComponent, { static: true }) createObjectComponent: CreateObjectComponent<ForumUser>;

  // add a component to view a item individually
  // ['id', 'forumUsername', 'roles'],
  constructor(public forumUserService: ForumUserService, public forumService: ForumService, public registeredUserService: RegisteredUserService,
    public dialog: MatDialog, public rolesService: RoleService) {
    super(
      [
        { name: "id" },
        { name: "forumUsername" },
        { name: "registeredUser", externalService: registeredUserService },
        { name: "roles", externalService: rolesService},
      ],
      [
        { inputType: 'text', name: 'forumUsername', controlsConfig: [null, Validators.required, null] },
        { inputType: 'foreign_pk', name: 'forum', controlsConfig: [null, null, null], externalService: forumService },
        { inputType: 'foreign_pk', name: 'registeredUser', controlsConfig: [null, Validators.required, null], externalService: registeredUserService },
      ]
    )
  }

  ngOnInit(): void {
    this.refreshView();
  }

  refreshView(): void {
    this.forumUserService.getAll().subscribe(
      r => {
        this.dataTableComponent.dataSource = r['content'];
      },
      err => console.log(err));
  }

  onRowClicked_View(item: ForumUser) {
    const dialogRef = this.dialog.open(ViewObjectComponent, {
      data: item,
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed', result);
    });
  }

  onRowClicked_ViewChild(item: AbstractModel) {
    const dialogRef = this.dialog.open(ViewObjectComponent, {
      data: item,
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed', result);
    });
  }

  onRowClicked_Edit(item: ForumUser) {
    this.createObjectComponent.startEdit(item);
    // this.createObjectComponent.createObjectForm.patchValue(item);
  }

  onRowClicked_Delete(item: ForumUser) {
    console.log('Delete clicked: ', item.id);
    this.forumUserService.delete(item.id).subscribe(r => {
      this.refreshView();
    });
  }
}
