import { Component, OnInit, ViewChild } from '@angular/core';
import { RegisteredUser } from 'src/app/models/registered-user';
import { DataTableComponent } from 'src/app/modular/components/data-table/data-table.component';
import { DataTableDataSource } from 'src/app/modular/data-table-datasource/data-table-datasource';
import { AbstractService } from 'src/app/modular/service/abstract-service';
import { RegisteredUserService } from 'src/app/services/registered-user/registered-user.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CreateObjectComponent } from 'src/app/modular/components/create-object/create-object.component';
import { FormBuilderInputRow } from 'src/app/modular/components/create-object/form-builder-input-row';
import { Validators } from '@angular/forms';
import { AbstractDBItem } from 'src/app/modular/abstract/abstract-db-item';
import { ForumUserService } from 'src/app/services/forum-user/forum-user.service';
import { AbstractModel } from 'src/app/models/abstract-model';
import { ViewObjectComponent } from 'src/app/modular/components/view-object/view-object.component';
import { MatDialog } from '@angular/material/dialog';


@Component({
  selector: 'app-registered-users',
  templateUrl: './registered-users.component.html',
  styleUrls: ['./registered-users.component.scss'],
  providers: [AbstractService]
})
export class RegisteredUsersComponent extends AbstractDBItem implements OnInit {

  @ViewChild(DataTableComponent, { static: true }) dataTableComponent: DataTableComponent<RegisteredUser, RegisteredUserService>;
  @ViewChild(CreateObjectComponent, { static: true }) createObjectComponent: CreateObjectComponent<RegisteredUser>;


  constructor(public registeredUserService: RegisteredUserService, public forumUserService: ForumUserService, private snackBar: MatSnackBar, public dialog: MatDialog) {
    super([
      { name: 'id' },
      { name: 'username' },
      { name: 'email' },
      { name: 'password' },
      { name: 'forumUsers', externalService: forumUserService }
    ],
      [
        { inputType: 'text', name: 'username', controlsConfig: [null, Validators.required, null] },
        { inputType: 'text', name: 'email', controlsConfig: [null, Validators.required, null] },
        { inputType: 'text', name: 'password', controlsConfig: [null, Validators.required, null] },
        // { inputType: 'text', name: 'forumUsers', controlsConfig: [null, Validators.required, null], externalService: forumUserService },
      ])
  }

  onRowClicked_ViewChild(item: AbstractModel): void {
    const dialogRef = this.dialog.open(ViewObjectComponent, {
      data: item,
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed', result);
    });
  }

  ngOnInit(): void {
    this.snackBar.open('Napomena: Prikazivanje lozinke je greska u smislu to nikada u praksi ne treba raditi; Ovde se prikazuje radi demonstrativne prirode.', 'Jasno');

    this.refreshView();
  }
 
  refreshView(): void {
    this.registeredUserService.getAll().subscribe(
      r => {
        this.dataTableComponent.dataSource = r['content'];
      },
      err => console.log(err));
  }

  onRowClicked_View(item: RegisteredUser) {
    console.log('View clicked: ', item);
  }

  onRowClicked_Edit(item: RegisteredUser) {
    console.log('Edit clicked: ', item);
    delete(item.forumUsers); 
    this.createObjectComponent.startEdit(item);
    // this.createObjectComponent.createObjectForm.patchValue(item);
  }

  onRowClicked_Delete(item: RegisteredUser) {
    console.log('Delete clicked: ', item.id);
    this.registeredUserService.delete(item.id).subscribe(r => {
      this.refreshView();
    });
  }
}
