import { Component, OnInit, ViewChild } from '@angular/core';
import { Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { AbstractModel } from 'src/app/models/abstract-model';
import { Forum } from 'src/app/models/forum';
import { AbstractDBItem } from 'src/app/modular/abstract/abstract-db-item';
import { CreateObjectComponent } from 'src/app/modular/components/create-object/create-object.component';
import { DataTableComponent } from 'src/app/modular/components/data-table/data-table.component';
import { ViewObjectComponent } from 'src/app/modular/components/view-object/view-object.component';
import { ForumUserService } from 'src/app/services/forum-user/forum-user.service';
import { ForumService } from 'src/app/services/forum/forum.service';
import { TopicService } from 'src/app/services/topic/topic.service';

@Component({
  selector: 'app-forum',
  templateUrl: './forum.component.html',
  styleUrls: ['./forum.component.scss']
})
export class ForumComponent extends AbstractDBItem implements OnInit {
  @ViewChild(DataTableComponent, { static: true }) dataTableComponent: DataTableComponent<Forum, ForumService>;
  @ViewChild(CreateObjectComponent, { static: true }) createObjectComponent: CreateObjectComponent<Forum>;

  //['id', 'name', 'isPublic', 'topics', 'forumUsers'],
  constructor(public forumService: ForumService, public topicService: TopicService, public forumUserService: ForumUserService, public dialog: MatDialog) {
    super(
      [
        { name: "id" },
        { name: "name" },
        { name: "isPublic" },
        { name: "topics", externalService: topicService },
        // { name: "forumUsers", externalService: forumUserService }
      ],
      [
        { inputType: 'text', name: 'name', controlsConfig: [null, Validators.required, null] },
        { inputType: 'boolean', name: 'isPublic', controlsConfig: [null, Validators.required, null] }
      ]
    )
  }

  ngOnInit(): void {
    this.refreshView();
  }

  refreshView(): void {
    this.forumService.getAll().subscribe(
      r => {
        this.dataTableComponent.dataSource = r['content'];
      },
      err => console.log(err));
  }

  onRowClicked_View(item: Forum) {
    const dialogRef = this.dialog.open(ViewObjectComponent, {
      data: item,
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed', result);
    });
  }

  onRowClicked_ViewChild(item: AbstractModel) {
    const dialogRef = this.dialog.open(ViewObjectComponent, {
      data: item,
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed', result);
    });
  }

  onRowClicked_Edit(item: Forum) {
    this.createObjectComponent.startEdit(item);
    // this.createObjectComponent.createObjectForm.patchValue(item);
  }

  onRowClicked_Delete(item: Forum) {
    console.log('Delete clicked: ', item.id);
    this.forumService.delete(item.id).subscribe(r => {
      this.refreshView();
    });
  }
}