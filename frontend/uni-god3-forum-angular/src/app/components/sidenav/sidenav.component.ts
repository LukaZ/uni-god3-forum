import { Component, OnInit } from '@angular/core';
import { animateText, onSideNavChange } from 'src/app/animations/animations';
import { LoginService } from 'src/app/services/login.service';

import { SidenavService } from '../../services/sidenav.service';

interface Page {
  link: string;
  name: string;
  icon: string;
}


@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
  animations: [onSideNavChange, animateText]
})
export class SidenavComponent implements OnInit {

  public sideNavState = false;
  public linkText = false;

  public pages: Page[] = [
    { name: 'Home', link: '', icon: 'home' },
    { name: 'Files', link: '/files', icon: 'description' },
    { name: 'Posts', link: '/posts', icon: 'inbox' },
    { name: 'Topics', link: '/topics', icon: 'lightbulb' },
    { name: 'Forums', link: '/forums', icon: 'article' },
    { name: 'Forum Users', link: '/forum-users', icon: 'manage_accounts' },
    { name: 'Registered Users', link: '/registered-users', icon: 'manage_accounts' },
    { name: 'Roles', link: '/roles', icon: 'assignment_ind' },
  ];

  constructor(private sidenavService: SidenavService, public loginService: LoginService) { }

  ngOnInit(): void {
  }

  onSinenavToggle(): void {
    this.sideNavState = !this.sideNavState;

    setTimeout(() => {
      this.linkText = this.sideNavState;
    }, 200);
    this.sidenavService.sideNavState$.next(this.sideNavState);
  }

}
