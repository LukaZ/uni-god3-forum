import { Component, OnInit, ViewChild } from '@angular/core';
import { Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';

import { CreateObjectComponent } from 'src/app/modular/components/create-object/create-object.component';
import { FormBuilderInputRow } from 'src/app/modular/components/create-object/form-builder-input-row';
import { DataTableComponent } from 'src/app/modular/components/data-table/data-table.component';
import { ViewObjectComponent } from 'src/app/modular/components/view-object/view-object.component';
import { FileService } from 'src/app/services/file/file.service';
import { AbstractDBItem } from 'src/app/modular/abstract/abstract-db-item';
import { AbstractModel } from 'src/app/models/abstract-model';
import { Role } from 'src/app/models/role';
import { RoleService } from 'src/app/services/role/role.service';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.scss']
})
export class RoleComponent extends AbstractDBItem implements OnInit {
  @ViewChild(DataTableComponent, { static: true }) dataTableComponent: DataTableComponent<Role, RoleService>;
  @ViewChild(CreateObjectComponent, { static: true }) createObjectComponent: CreateObjectComponent<Role>;

  // add a component to view a item individually

  constructor(public roleService: RoleService, public dialog: MatDialog) {
    super(
      [
        { name: "id" },
        { name: "name" }
      ],
      [
        { inputType: 'text', name: 'name', controlsConfig: [null, Validators.required, null] }
      ]
    )
  }

  ngOnInit(): void {
    this.refreshView();
  }

  refreshView(): void {
    this.roleService.getAll().subscribe(
      r => {
        this.dataTableComponent.dataSource = r['content'];
      },
      err => console.log(err));
  }

  onRowClicked_View(item: Role) {
    const dialogRef = this.dialog.open(ViewObjectComponent, {
      data: item,
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed', result);
    });
  }

  onRowClicked_ViewChild(item: AbstractModel) {
    const dialogRef = this.dialog.open(ViewObjectComponent, {
      data: item,
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed', result);
    });
  }

  onRowClicked_Edit(item: Role) {
    this.createObjectComponent.startEdit(item);
    // this.createObjectComponent.createObjectForm.patchValue(item);
  }

  onRowClicked_Delete(item: Role) {
    console.log('Delete clicked: ', item.id);
    this.roleService.delete(item.id).subscribe(r => {
      this.refreshView();
    });
  }
}
