import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  // emailRegx = /^(([^<>+()\[\]\\.,;:\s@"-#$%&=]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,3}))$/;

  constructor(private formBuilder: FormBuilder, private loginService: LoginService, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      // email: [null, [Validators.required, Validators.pattern(this.emailRegx)]],
      username: [null, Validators.required],
      password: [null, Validators.required]
    });

  }

  submit(): void {

    if (!this.loginForm.valid) {
      return;
    }

    let res = this.loginService.login(this.loginForm.value).subscribe(r => {
    }, error => {
      this.snackBar.open('Neuspesan login', '', { duration: 1000,  panelClass: ['mat-toolbar', 'mat-warn'] });
    });
  }

}
