import { Component, OnInit, ViewChild } from '@angular/core';
import { Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Post } from 'src/app/models/post';
import { AbstractDBItem } from 'src/app/modular/abstract/abstract-db-item';
import { CreateObjectComponent } from 'src/app/modular/components/create-object/create-object.component';
import { DataTableComponent } from 'src/app/modular/components/data-table/data-table.component';
import { ViewObjectComponent } from 'src/app/modular/components/view-object/view-object.component';
import { FileService } from 'src/app/services/file/file.service';
import { PostService } from 'src/app/services/post/post.service';
import { TopicService } from 'src/app/services/topic/topic.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent extends AbstractDBItem implements OnInit {

  @ViewChild(DataTableComponent, { static: true }) dataTableComponent: DataTableComponent<Post, PostService>;
  @ViewChild(CreateObjectComponent, { static: true }) createObjectComponent: CreateObjectComponent<Post>;

  // add a component to view a item individually 
  // ['id', 'content', 'publicationDate', 'topic', 'files'],
  constructor(public postService: PostService, public topicService: TopicService, public fileService: FileService, public dialog: MatDialog) {
    super(
      [
        { name: "id" },
        { name: "content" },
        { name: "publicationDate" },
        { name: "topic", externalService: topicService },
        { name: "files", externalService: fileService }
      ],
      [
        { inputType: 'text', name: 'content', controlsConfig: [null, Validators.required, null] },
        { inputType: 'date', name: 'publicationDate', controlsConfig: [null, Validators.required, null] },
        { inputType: 'foreign_pk', name: 'topic', controlsConfig: [null, null, null],  externalService: topicService}
      ]
    )
  }

  ngOnInit(): void {
    this.refreshView();
  }

  refreshView(): void {
    this.postService.getAll().subscribe(
      r => {
        this.dataTableComponent.dataSource = r['content'];
      },
      err => console.log(err));
  }

  onRowClicked_View(item: Post) {
    const dialogRef = this.dialog.open(ViewObjectComponent, {
      data: item,
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  onRowClicked_ViewChild(item: Post) {
    const dialogRef = this.dialog.open(ViewObjectComponent, {
      data: item,
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  onRowClicked_Edit(item: Post) {
    this.createObjectComponent.startEdit(item);
    // this.createObjectComponent.createObjectForm.patchValue(item);
  }

  onRowClicked_Delete(item: Post) {
    this.postService.delete(item.id).subscribe(r => {
      this.refreshView();
    });
  }
}
