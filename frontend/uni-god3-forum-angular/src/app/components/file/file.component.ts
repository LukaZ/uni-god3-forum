import { Component, OnInit, ViewChild } from '@angular/core';
import { Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { File } from 'src/app/models/file';
import { Post } from 'src/app/models/post';
import { CreateObjectComponent } from 'src/app/modular/components/create-object/create-object.component';
import { FormBuilderInputRow } from 'src/app/modular/components/create-object/form-builder-input-row';
import { DataTableComponent } from 'src/app/modular/components/data-table/data-table.component';
import { ViewObjectComponent } from 'src/app/modular/components/view-object/view-object.component';
import { FileService } from 'src/app/services/file/file.service';
import { AbstractDBItem } from 'src/app/modular/abstract/abstract-db-item';
import { AbstractModel } from 'src/app/models/abstract-model';
import { PostService } from 'src/app/services/post/post.service';

@Component({
  selector: 'app-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.scss']
})
export class FileComponent extends AbstractDBItem implements OnInit {
  @ViewChild(DataTableComponent, { static: true }) dataTableComponent: DataTableComponent<File, FileService>;
  @ViewChild(CreateObjectComponent, { static: true }) createObjectComponent: CreateObjectComponent<File>;

  // add a component to view a item individually
  // ['id', 'url', 'description', 'post'],
  constructor(public fileService: FileService, public postService: PostService, public dialog: MatDialog) {
    super(
      [
        { name: "id" },
        { name: "url" },
        { name: "description" },
        { name: "post", externalService: postService }
      ],
      [
        { inputType: 'text', name: 'url', controlsConfig: [null, Validators.required, null] },
        { inputType: 'text', name: 'description', controlsConfig: [null, Validators.required, null] },
        { inputType: 'foreign_pk', name: 'post', controlsConfig: [null, Validators.required, null], externalService: postService },
      ]
    )
  }

  ngOnInit(): void {
    this.refreshView();
  }

  refreshView(): void {
    this.fileService.getAll().subscribe(
      r => {
        this.dataTableComponent.dataSource = r['content'];
      },
      err => console.log(err));
  }

  onRowClicked_View(item: File) {
    const dialogRef = this.dialog.open(ViewObjectComponent, {
      data: item,
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed', result);
    });
  }

  onRowClicked_ViewChild(item: AbstractModel) {
    const dialogRef = this.dialog.open(ViewObjectComponent, {
      data: item,
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed', result);
    });
  }

  onRowClicked_Edit(item: File) {
    this.createObjectComponent.startEdit(item);
    // this.createObjectComponent.createObjectForm.patchValue(item);
  }

  onRowClicked_Delete(item: File) {
    console.log('Delete clicked: ', item.id);
    this.fileService.delete(item.id).subscribe(r => {
      this.refreshView();
    });
  }
}
