import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from '../services/login.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private loginService: LoginService, private router: Router, private snackBar: MatSnackBar) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.loginService.validateRoles(route.data.allowedRoles)) {
      return true;
    }


    if (this.loginService.isLoggedIn()) {
      this.snackBar.open('You do not have the required roles to view this page', 'Ok');
    } else {
      this.loginService.attemptLogin();
      this.snackBar.open('You need to be logged in to view this page');
      this.router.createUrlTree(['/login']);
    }
    return false;
  }
}
