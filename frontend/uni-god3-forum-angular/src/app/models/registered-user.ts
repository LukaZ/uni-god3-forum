import { AbstractModel } from './abstract-model';
import { ForumUser } from './forum-user';

export interface RegisteredUser extends AbstractModel {
    username: string;
    email: string;
    forumUsers: ForumUser[];
}
