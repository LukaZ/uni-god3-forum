import { AbstractModel } from './abstract-model';
import { Forum } from './forum';
import { Post } from './post';

export interface Topic extends AbstractModel{
    topicName: string;
    forum: Forum;
    posts: Post[];
}
