import { AbstractModel } from './abstract-model';
import { ForumUser } from './forum-user';
import { Topic } from './topic';

export interface Forum extends AbstractModel {
    name: string;
    isPublic: boolean;
    topics: Topic[];
    forumUsers: ForumUser[];
}
