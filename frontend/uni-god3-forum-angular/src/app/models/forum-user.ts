import { AbstractModel } from './abstract-model';
import { Forum } from './forum';
import { RegisteredUser } from './registered-user';
import { Role } from './role';

export interface ForumUser extends AbstractModel{
    registeredUser: RegisteredUser;
    forumUsername: string;
    roles: Role[];
    forum: Forum;
}
