import { AbstractModel } from './abstract-model';
import { File } from './file';

export interface Post extends AbstractModel {
    publicationDate: Date;
    content: string;
    files: File[];
}
