import { AbstractModel } from './abstract-model';

export interface AuthResponse extends AbstractModel{
    jwt: string;
}
