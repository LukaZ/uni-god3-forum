import { AbstractModel } from "./abstract-model";
import { ForumUser } from "./forum-user";

export interface Role extends AbstractModel{
    name: string;
    forumUsers: ForumUser[];
}
