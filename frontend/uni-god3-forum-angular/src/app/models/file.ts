import { AbstractModel } from './abstract-model';
import { Post } from './post';

export interface File extends AbstractModel{
    description: string;
    url: string;
    post: Post;
}
