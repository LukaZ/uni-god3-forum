export interface JWTClaims {
    sub: string;
    created: number;
    exp: number;
    authorities: string[];
}
