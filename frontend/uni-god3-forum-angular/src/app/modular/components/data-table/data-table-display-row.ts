import { AbstractModel } from "src/app/models/abstract-model";
import { AbstractService } from "../../service/abstract-service";

export interface DataTableDisplayRow {
    name: string;
    // inputType: string;
    // actions: string[];
    externalService?: AbstractService<AbstractModel>;
    externalServiceData?: any[];
}
