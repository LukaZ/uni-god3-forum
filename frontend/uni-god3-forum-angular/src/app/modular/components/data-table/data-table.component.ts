import { EventEmitter } from '@angular/core';
import { AfterViewInit, Component, Inject, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { AbstractDBItem } from '../../abstract/abstract-db-item';
import { DataTableDataSource } from '../../data-table-datasource/data-table-datasource';
import { AbstractService } from '../../service/abstract-service';
import { FormBuilderInputRow } from '../create-object/form-builder-input-row';
import { DataTableDisplayRow } from './data-table-display-row';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss'],
})
export class DataTableComponent<M, S extends AbstractService<M>> implements OnInit {
  refreshView(): void {
    throw new Error('Method not implemented.');
  }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  // @ViewChild(MatTable) table: MatTable<M>;

  // dataSource: DataTableDataSource<M>;
  dataSource: M[] = [];
  // dataService: S;

  @Input()
  displayedColumns: DataTableDisplayRow[];

  @Output()
  viewClick: EventEmitter<M> = new EventEmitter<M>();

  @Output()
  viewClickChild: EventEmitter<M> = new EventEmitter<M>();

  @Output()
  editClick: EventEmitter<M> = new EventEmitter<M>();

  @Output()
  deleteClick: EventEmitter<M> = new EventEmitter<M>();


  constructor() { }


  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */

  //[length]="dataSource.data.length"

  ngOnInit() {
    // this.displayedColumns.push('actions');
    console.log(this.displayedColumns);
    this.displayedColumns.push({ name: "actions" }) // for the delete, edit, etc buttons
  }

  getColumnsToString() {

    try {

      let dipc = []
      this.displayedColumns.forEach(element => {
        dipc.push(element.name);
      });
      return dipc;
    } catch (error) {
      console.log(error)
      // TODO: This really need to be fixed ... 
      return ['Error getting columns!, most likely due to failing to pass this.displayColumns in parent template!'];
    }

  }

  ngAfterViewInit() {
  }

  onRowClicked_View(item: M) {
    // console.log('View clicked: ', item);
    this.viewClick.emit(item);
  }

  onRowClicked_ViewChild(item: M) {
    // console.log('View clicked: ', item);
    this.viewClickChild.emit(item);
  }


  onRowClicked_Edit(item: M) {
    // console.log('Edit clicked: ', item);
    this.editClick.emit(item);
  }

  onRowClicked_Delete(item: M) {
    // console.log('Delete clicked: ', item);
    this.deleteClick.emit(item);
  }

  // Helper function to check if a cell is a object ( a linked element, if so make it a button)
  isObject(val): boolean { return typeof val === 'object'; }

  // onRowClicked(row) {
  //   console.log('Row clicked: ', row);
  // }
}
