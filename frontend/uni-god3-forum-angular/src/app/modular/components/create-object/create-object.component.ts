import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { AbstractModel } from 'src/app/models/abstract-model';
import { AbstractService } from '../../service/abstract-service';
import { FormBuilderInputRow } from './form-builder-input-row';

@Component({
  selector: 'app-create-object',
  templateUrl: './create-object.component.html',
  styleUrls: ['./create-object.component.scss']
})
export class CreateObjectComponent<M> implements OnInit {

  createObjectForm: FormGroup;
  formattedInputs = [];

  isEditing: boolean = false;
  editingItemID: number; // The ID of the item we are editing

  // Indicates weather or not we have bi-directional objects ( fk keys ) in the relation
  hasBidirectionalObjects: boolean = false;

  // Array that holds the sub item(s) contained inside the main item that is being edited
  //  we can only edit the ID to be different but we keep the original values and replace them
  //  back into the original object when the edit is completed ?
  bidirectionalItemHolder = [];

  // XML File uploading
  fileToUpload: File ;

  constructor(private formBuilder: FormBuilder) { }

  @Input() // The service that works in the background
  inputService: AbstractService<M>;

  @Input() // Input that tells us what inputs to put in the form
  createObjectFields: FormBuilderInputRow[];

  @Output() // Called when we are finished creating a new item
  submitClick: EventEmitter<M> = new EventEmitter<M>();

  @Output() // Called when we are finished editing a existing field
  updateClick: EventEmitter<M> = new EventEmitter<M>();


  ngOnInit(): void {
    // format the fields for input
    for (let i = 0; i < this.createObjectFields.length; i++) {
      let element = this.createObjectFields[i];
      this.formattedInputs[element.name] = element.controlsConfig;

      if (element.hasOwnProperty('externalService')) {
        element.externalService.getAll().subscribe(r => {
          element.externalServiceData = r['content'];
        });
      }
    }
    this.createObjectForm = this.formBuilder.group(this.formattedInputs);
  }

  startEdit(item: M) {
    this.isEditing = true;

    this.editingItemID = item['id'];
    this.createObjectForm.patchValue(item);

    // Check if we are being passed a Object which has a id field
    Object.keys(item).forEach(editingItemKey => {
      if (typeof item[editingItemKey] === 'object') { // key is 'post', or 'file' which is a obj.

        this.hasBidirectionalObjects = true;
        // We need to store the object into a a placeholder so that we can handle the ID latter
        this.bidirectionalItemHolder[editingItemKey] = { 'key': editingItemKey, 'value': item[editingItemKey] }
        // Extract the id out of the object and put it in the approperiate field so we dont have [Object]
        //   in the params
        this.createObjectForm.patchValue({ [editingItemKey]: item[editingItemKey] })

      }
    });
  }

  onFileSelected(event) {
    const file: File = event.target.files[0];
    if (file) {
      this.fileToUpload = file;
    }
  }

  uploadFile(){
    if (this.fileToUpload){
      // We need to use form data here because on the API end we expect a Multipart request
      var fd = new FormData();
      fd.append('file', this.fileToUpload);
      this.inputService.uploadXML(fd).subscribe(r => { this.submitClick.emit(r); });
      this.createObjectForm.reset()
    }
  }

  _getObjectKeys(obj) { return Object.keys(obj) };

  cancelEdit() {
    this.createObjectForm.reset();
    this.isEditing = false;
    this.editingItemID = null;
    this.hasBidirectionalObjects = false;
  }


  submit(): void {
    if (!this.createObjectForm.valid) {
      return;
    }
    console.log(this.createObjectForm.value);

    // Before we send the data we need to go over all the controls that have a date as a input
    //  then convert that to timestamp format
    // TODO: Make the angular form accept unix timestamp as a acceptable input ( it comes out as ivalid on timestamp by default)


    if (this.isEditing) { // Are we editing a existing row ?
      this.createObjectForm.value['id'] = this.editingItemID;

      // Set the sub objects back to being objects and not the id of said sub object
      for (const _bidirectionalItemKey in this.bidirectionalItemHolder) {
        let _bidirectionalItemValue = this.bidirectionalItemHolder[_bidirectionalItemKey]
        this.createObjectForm.value[_bidirectionalItemKey] = _bidirectionalItemValue['value']
      }

      this.inputService.update(this.createObjectForm.value).subscribe(r => { this.updateClick.emit(r); });
      this.createObjectForm.reset();
    } else { // No, we are creating a new row
      this.inputService.create(this.createObjectForm.value).subscribe(r => { this.submitClick.emit(r); });
      this.createObjectForm.reset();
    }
  }
}
