// This interface defines the values a input field should contain when being passed to create-object.

import { AbstractModel } from "src/app/models/abstract-model";
import { AbstractService } from "../../service/abstract-service";

export interface FormBuilderInputRow{
    name: string;
    inputType: string;
    controlsConfig: any[];
    validatorRegex?: RegExp;
    externalService?: AbstractService<AbstractModel>;
    externalServiceData?: any[];
}
