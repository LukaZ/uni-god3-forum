import { Component, OnInit, Inject, Input } from '@angular/core';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AbstractModel } from 'src/app/models/abstract-model';

@Component({
  selector: 'app-view-object',
  templateUrl: './view-object.component.html',
  styleUrls: ['./view-object.component.scss']
})
export class ViewObjectComponent implements OnInit {

  // The keys of data, stored in variable for ease of use
  viewModelKeys: string[] 
  
  constructor(
    public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: Array<AbstractModel>) {

  }

  ngOnInit(): void {
    this.viewModelKeys = this.data.length >= 1 ? Object.keys(this.data[0]) : Object.keys(this.data);
  }

  onOkClick(): void {
    this.dialog.closeAll()
  }

  _isDataArray(): boolean {
    return this.data.constructor === Array;
  }
}
