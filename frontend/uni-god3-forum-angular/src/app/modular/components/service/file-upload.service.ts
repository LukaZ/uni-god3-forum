import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {

  constructor(public httpClient: HttpClient) { }

  postFile(fileToUpload: File): void {
    const endpoint = 'your-destination-url';
    const formData: FormData = new FormData();
    formData.append('fileKey', fileToUpload, fileToUpload.name);
    let result;
    this.httpClient.post(endpoint, formData).subscribe(r => {
      result = r;
      console.log('ok')
    })
  }

  handleError(e: any) {
    throw new Error('Method not implemented.');
  }
}
