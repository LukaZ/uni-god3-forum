import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Inject, Injectable } from '@angular/core';

@Injectable()
export class AbstractService<M> {

    httpEndpoint: string;

    constructor(public _httpClient: HttpClient, @Inject(String) private _httpEndPoint: string) {
        this.httpEndpoint = 'http://localhost:8080/api' + _httpEndPoint;
    }

    getAll(): Observable<M[]> {
        return this._httpClient.get<M[]>(this.httpEndpoint);
    }

    getOne(id: number): Observable<M> {
        return this._httpClient.get<M>(`${this.httpEndpoint}/${id}`);
    }

    create(product: M): Observable<M> {
        return this._httpClient.post<M>(this.httpEndpoint, product);
    }

    update(product: M): Observable<M> {
        return this._httpClient.put<M>(`${this.httpEndpoint}`, product);
    }

    delete(id: number): Observable<M> {
        return this._httpClient.delete<M>(`${this.httpEndpoint}/${id}`);
    }

    uploadXML(file: FormData): Observable<M> {
        return this._httpClient.post<M>(`${this.httpEndpoint}/xml`, file);
    }
}
