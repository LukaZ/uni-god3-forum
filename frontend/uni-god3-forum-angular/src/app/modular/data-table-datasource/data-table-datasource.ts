import { AbstractService } from '../service/abstract-service';
import { DataSource } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { map } from 'rxjs/operators';
import { Observable, of as observableOf, merge } from 'rxjs';




export class DataTableDataSource<M> extends DataSource<M> {
    model: M;
    data: M[];

    constructor(private paginator: MatPaginator, private sort: MatSort) {
        super();
    }

    /**
     * Connect this data source to the table. The table will only update when
     * the returned stream emits new items.
     * @returns A stream of the items to be rendered.
     */
    connect(): Observable<M[]> {
        // Combine everything that affects the rendered data into one update
        // stream for the data-table to consume.
        // array
        console.log('called!', this.data);
        const dataMutations = [
            observableOf(this.data),
            this.paginator.page,
            this.sort?.sortChange,
        ];

        // Set the paginator's length
        this.paginator.length = this.getDataLeinght(); //this.data.length;

        // transform array into a sorted copy
        return merge(...dataMutations).pipe(
            map(() => {
                return this.getPagedData(this.getSortedData([...this.data]));
            })
        );
    }

    getDataLeinght(): number {
        return this.data?.length > 0 ? this.data.length : 0;
    }

    /**
     *  Called when the table is being destroyed. Use this function, to clean up
     * any open connections or free any held resources that were set up during connect.
     */
    disconnect() { }

    /**
     * Paginate the data (client-side). If you're using server-side pagination,
     * this would be replaced by requesting the appropriate data from the server.
     */
    private getPagedData(data: M[]) {
        const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
        return data.splice(startIndex, this.paginator.pageSize);
    }

    /**
     * Sort the data (client-side). If you're using server-side sorting,
     * this would be replaced by requesting the appropriate data from the server.
     */
    private getSortedData(data: M[]) {
        if (!this.sort.active || this.sort.direction === '') {
            return data;
        }

        return data.sort((a, b) => {
            const isAsc = this.sort.direction === 'asc';
            switch (this.sort.active) {
                // case 'name':
                //     return compare(a.name, b.name, isAsc);
                case 'id':
                    return compare(+a['id'], +b['id'], isAsc);
                default:
                    return 0;
            }
        });
    }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a: any, b: any, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}