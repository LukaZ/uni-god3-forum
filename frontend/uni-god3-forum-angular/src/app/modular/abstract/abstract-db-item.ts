import { AbstractModel } from "src/app/models/abstract-model";
import { FormBuilderInputRow } from "../components/create-object/form-builder-input-row";
import { DataTableDisplayRow } from "../components/data-table/data-table-display-row";
//  Small helper 
export abstract class AbstractDBItem {
    displayedColumns: DataTableDisplayRow[]; // We send this to app-data-table to know what collumns to display 
    createObjectFields: FormBuilderInputRow[]  // We send this to app-create-object so that it knows what values it should use for object creation/editing

    constructor( displayedColumns: DataTableDisplayRow[], createObjectFields: FormBuilderInputRow[]){
        this.displayedColumns = displayedColumns;
        this.createObjectFields = createObjectFields
    }

    // Called when we need to refresh the items in the table
    abstract refreshView(): void;
    
    //Called when we click the view item button
    abstract onRowClicked_View(item: AbstractModel): void;
    
    //Called when we click the view item button on a sub item linked to a entry
    abstract onRowClicked_ViewChild(item: AbstractModel): void;
    
    //Called when we click the edit item button
    abstract onRowClicked_Edit(item: AbstractModel): void;
    
    //Called when we click the delete item button
    abstract onRowClicked_Delete(item: AbstractModel): void;
}