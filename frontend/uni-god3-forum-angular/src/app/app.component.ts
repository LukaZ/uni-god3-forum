import { Component, OnInit } from '@angular/core';
import { onMainContentChange } from './animations/animations';
import { LoginService } from './services/login.service';
import { PreloaderService } from './services/preloader.service';
import { SidenavService } from './services/sidenav.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [onMainContentChange]
})
export class AppComponent implements OnInit {
  title = 'uni-god3-forum-angular';

  public onSideNavChange = false;

  constructor(private sidenavService: SidenavService, private preloader: PreloaderService, private loginService: LoginService) {
    this.sidenavService.sideNavState$.subscribe(res => {
      this.onSideNavChange = res;
    });
  }

  ngOnInit(): void {
    this.loginService.attemptLogin();
  }

  ngAfterViewInit() {
    this.preloader.hide();
  }

}
