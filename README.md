# uni-god3-forum

Upotrebljeni lombok za olaksavanje pisanje koda, testiranje putem Swagger.

# Prezentacija

## Uvod
Za projekat sam izabrao forum za studente

## Tabela za prikaz
Napravljen za modularni pristup CRUD operacija. Takodje je omoguceno da povezani entitete mozemo prikazati pritiskom na dugme "View" ukoliko postoje. Sa desne strane se nalaze dugma za prikaz entiteta, modifikaciju i brisanje samog entiteta.

## Forma za kreiranje i editovanje entiteta
Takodjer napravljen za modularni pristup. Komponenta takodje podrzava mogucnost da preko drop down selektujemo eksterne entitete za koje je vezan prvi entitet. Prilikom Klika dugmeta za editovanje entiteta forma se pretvara u edit formu gde mozemo menjati originalni entitet.

## Autentifikacija
Se vrsi putem JWT tokena i skladisti se u kolacicima pod ime "token"

## Kontrola rola
Za neke od entiteta da bi pristupili stranici za CRUD operacije potrebno je da posedujemo odgovarajuce role tj. da budemo ulogovani na admin/user nalog 

# Checks 

Files:
    C = OK 
    R = OK 
    U = OK
    D = OK
Posts:
    C = OK
    R = OK
    U = OK
    D = OK
Topics:
    C = OK
    R = OK
    U = OK
    D = OK
Forums:
    C = OK
    R = OK
    U = OK
    D = OK
Forum Users:
    C = OK
    R = OK
    U = OK
    D = OK
Registered Users:
    C = OK
    R = OK
    U = OK
    D = OK
Roles:
    C = OK
    R = OK
    U = OK
    D = OK



