FROM openjdk:11.0-jdk
COPY target/uni-god3-forum-0.0.1-SNAPSHOT.jar uni-god3-forum-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/uni-god3-forum-0.0.1-SNAPSHOT.jar"]