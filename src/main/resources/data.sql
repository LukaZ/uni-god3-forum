INSERT INTO forum (is_public, name) VALUES (true, 'class');
INSERT INTO forum (is_public, name) VALUES (true, 'support');

INSERT INTO registered_user (email, username, password) VALUES ('user@test.com', 'user', '{bcrypt}$2a$10$y3lttskhhbRZ5rMvmXRwROomtXUHEgH1ACAG6vvVnD4IalchNRA.m');
INSERT INTO registered_user (email, username, password) VALUES ('admin@test.com', 'admin', '{bcrypt}$2a$10$y3lttskhhbRZ5rMvmXRwROomtXUHEgH1ACAG6vvVnD4IalchNRA.m');

INSERT INTO forum_user (forum_username, forum_id, registered_user_id) VALUES ('user', 1, 1);
INSERT INTO forum_user (forum_username, forum_id, registered_user_id) VALUES ('admin', 1, 2);

INSERT INTO role (name) VALUES ('ROLE_GUEST');
INSERT INTO role (name) VALUES ('ROLE_USER');
INSERT INTO role (name) VALUES ('ROLE_ADMIN');

-- Give the forum user their roles
INSERT INTO forum_user_roles(roles_id, forum_users_id) VALUES (1, 1);
INSERT INTO forum_user_roles(roles_id, forum_users_id) VALUES (2, 1);
INSERT INTO forum_user_roles(roles_id, forum_users_id) VALUES (3, 2);

---- Assign what forum user belongs to which forum
--INSERT INTO forum_forum_users(forum_id, forum_users_id) VALUES (1, 1);
--INSERT INTO forum_forum_users(forum_id, forum_users_id) VALUES (2, 2);

INSERT INTO topic(topic_name, forum_id) VALUES ('Grade Results', 1);
INSERT INTO topic(topic_name, forum_id) VALUES ('Homework', 1);
INSERT INTO topic(topic_name, forum_id) VALUES ('Lectures', 1);
INSERT INTO topic(topic_name, forum_id) VALUES ('Broken Chromebooks', 2);
INSERT INTO topic(topic_name, forum_id) VALUES ('Lab Equipment', 2);
INSERT INTO topic(topic_name, forum_id) VALUES ('School Supplies', 2);

---- Assign the above topics to the respective forum ( class forum gets the topics grade results, homework, etc )
--INSERT INTO forum_topics(forum_id, topics_id) VALUES (1, 1);
--INSERT INTO forum_topics(forum_id, topics_id) VALUES (1, 2);
--INSERT INTO forum_topics(forum_id, topics_id) VALUES (1, 3);
--INSERT INTO forum_topics(forum_id, topics_id) VALUES (2, 4);
--INSERT INTO forum_topics(forum_id, topics_id) VALUES (2, 5);
--INSERT INTO forum_topics(forum_id, topics_id) VALUES (2, 6);

INSERT INTO post(content, publication_date, topic_id) VALUES ('content 1', now(), 1);
INSERT INTO post(content, publication_date, topic_id) VALUES ('content 2', now(), 2);
INSERT INTO post(content, publication_date, topic_id) VALUES ('content 3', now(), 3);
INSERT INTO post(content, publication_date, topic_id) VALUES ('content 4', now(), 4);
INSERT INTO post(content, publication_date, topic_id) VALUES ('content 5', now(), 5);
INSERT INTO post(content, publication_date, topic_id) VALUES ('content 6', now(), 6);

INSERT INTO file(description, url, post_id) VALUES ('description11', 'url11', 1);
INSERT INTO file(description, url, post_id) VALUES ('description12', 'url12', 1);
INSERT INTO file(description, url, post_id) VALUES ('description13', 'url13', 1);
INSERT INTO file(description, url, post_id) VALUES ('description2', 'url2', 2);
INSERT INTO file(description, url, post_id) VALUES ('description3', 'url3', 3);
INSERT INTO file(description, url, post_id) VALUES ('description4', 'url4', 4);
INSERT INTO file(description, url, post_id) VALUES ('description5', 'url5', 5);
--INSERT INTO file(description, url, post_id) VALUES ('description 6', 'localhost', 6);