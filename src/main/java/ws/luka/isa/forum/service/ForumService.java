package ws.luka.isa.forum.service;

import org.springframework.stereotype.Service;
import ws.luka.isa.forum.dto.ForumDTO;
import ws.luka.isa.forum.model.Forum;
import ws.luka.isa.forum.repository.ForumRepository;

@Service
public class ForumService extends AbstractService<ForumDTO, Forum, ForumRepository> {
}
