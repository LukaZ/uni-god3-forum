package ws.luka.isa.forum.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ws.luka.isa.forum.model.ForumUser;
import ws.luka.isa.forum.model.RegisteredUser;
import ws.luka.isa.forum.model.Role;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Optional;

@Slf4j
@Service
public class UserDetailServiceImpl implements UserDetailsService {
    @Autowired
    ForumUserService forumUserService;

    @Autowired
    RegisteredUserService registeredUserService;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //Fetch user by their username
        //TODO It would be a smart idea to have a class that is a parent to both these 2 classes so that we don't have
        //  to define 2 different variables depending on if a ForumUser exists.
        Optional<ForumUser> forumUser = forumUserService.findByUsername(username);
        Optional<RegisteredUser> registeredUser = registeredUserService.findByUsername(username);


        ArrayList<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();

        if (forumUser.isPresent()) {
            for (Role role : forumUser.get().getRoles()) {
                grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));
            }

            // Create the user based off the username, password and roles("granted authorities")
            return new User(forumUser.get().getForumUsername(), forumUser.get().getRegisteredUser().getPassword(),
                    grantedAuthorities);
        } else if (registeredUser.isPresent()) {
            // This ForumUser does not exist! So create one based off the RegisteredUser
            return new User(registeredUser.get().getUsername(), registeredUser.get().getPassword(), grantedAuthorities);
        }

        final String errorMsg = String.format("No RegisteredUser or ForumUser with \"%s\" exist", username);
        log.error(errorMsg);
        throw new UsernameNotFoundException(errorMsg);
    }

}
