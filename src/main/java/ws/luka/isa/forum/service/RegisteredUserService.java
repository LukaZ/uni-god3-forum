package ws.luka.isa.forum.service;

import org.springframework.stereotype.Service;
import ws.luka.isa.forum.dto.RegisteredUserDTO;
import ws.luka.isa.forum.model.RegisteredUser;
import ws.luka.isa.forum.repository.RegisteredUserRepository;

import java.util.Optional;

@Service
public class RegisteredUserService extends AbstractService<RegisteredUserDTO, RegisteredUser, RegisteredUserRepository> {

    public Optional<RegisteredUser> findByUsername(String userName) {
        return repository.findByUsername(userName);
    }

    public Optional<RegisteredUser> findByEmail(String email) {
        return repository.findByEmail(email);
    }

}
