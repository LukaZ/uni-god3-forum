package ws.luka.isa.forum.service;

import org.springframework.stereotype.Service;
import ws.luka.isa.forum.dto.PostDTO;
import ws.luka.isa.forum.model.Post;
import ws.luka.isa.forum.repository.PostRepository;

@Service
public class PostService extends AbstractService<PostDTO, Post, PostRepository> {
}
