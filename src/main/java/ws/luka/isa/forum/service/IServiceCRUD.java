package ws.luka.isa.forum.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface IServiceCRUD<MODEL> {
//    public Iterable<V> findAll();
    Page<MODEL> findAll(Pageable pageable);
    Optional<MODEL> findOne(Long id);
    void delete(Long id);
    void delete(MODEL item);
    MODEL save(MODEL item);
}
