package ws.luka.isa.forum.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ws.luka.isa.forum.dto.AbstractDTO;
import ws.luka.isa.forum.model.AbstractModel;
import ws.luka.isa.forum.repository.AbstractedRepository;

import java.util.Optional;

public abstract class AbstractService<D extends AbstractDTO, M extends AbstractModel<D>, R extends AbstractedRepository<D, M>> implements IServiceCRUD<M> {
    @Autowired
    R repository;

    @Override
    public Page<M> findAll(Pageable pageable) {
        return repository.findAll(pageable);
    }

    @Override
    public Optional<M> findOne(Long id) { return repository.findById(id); }

    @Override
    public M save(M item) { return repository.save(item); }

    @Override
    public void delete(Long id) {
        repository.deleteById(id);
    }

    @Override
    public void delete(M item) {
        repository.delete(item);
    }
}
