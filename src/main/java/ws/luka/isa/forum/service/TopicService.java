package ws.luka.isa.forum.service;

import org.springframework.stereotype.Service;
import ws.luka.isa.forum.dto.TopicDTO;
import ws.luka.isa.forum.model.Topic;
import ws.luka.isa.forum.repository.TopicRepository;

@Service
public class TopicService extends AbstractService<TopicDTO, Topic, TopicRepository> {
}
