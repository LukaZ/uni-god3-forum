package ws.luka.isa.forum.service;

import org.springframework.stereotype.Service;
import ws.luka.isa.forum.dto.RoleDTO;
import ws.luka.isa.forum.model.Role;
import ws.luka.isa.forum.repository.RoleRepository;

@Service
public class RoleService extends AbstractService<RoleDTO, Role, RoleRepository> {
}
