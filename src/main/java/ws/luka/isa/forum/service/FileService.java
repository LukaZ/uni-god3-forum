package ws.luka.isa.forum.service;


import lombok.Data;
import org.springframework.stereotype.Service;
import ws.luka.isa.forum.dto.FileDTO;
import ws.luka.isa.forum.model.File;
import ws.luka.isa.forum.repository.FileRepository;


@Data
@Service
public class FileService extends AbstractService<FileDTO, File, FileRepository> {
}

