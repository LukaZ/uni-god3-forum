package ws.luka.isa.forum.service;

import lombok.Data;
import org.springframework.stereotype.Service;
import ws.luka.isa.forum.dto.ForumUserDTO;
import ws.luka.isa.forum.model.ForumUser;
import ws.luka.isa.forum.repository.ForumUserRepository;

import java.util.Optional;

@Service
@Data
public class ForumUserService extends AbstractService<ForumUserDTO, ForumUser, ForumUserRepository> {

    public Optional<ForumUser> findByUsername(String forumUsername) {
        return repository.findByForumUsername(forumUsername);
    }
}

