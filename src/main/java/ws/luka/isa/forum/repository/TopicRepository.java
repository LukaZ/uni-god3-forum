package ws.luka.isa.forum.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ws.luka.isa.forum.dto.TopicDTO;
import ws.luka.isa.forum.model.Topic;

@Repository
public interface TopicRepository extends AbstractedRepository<TopicDTO, Topic> {
}
