package ws.luka.isa.forum.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ws.luka.isa.forum.dto.FileDTO;
import ws.luka.isa.forum.model.File;

@Repository
public interface FileRepository extends AbstractedRepository<FileDTO, File>{ //PagingAndSortingRepository<File, Long> {
}





