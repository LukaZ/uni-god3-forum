package ws.luka.isa.forum.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ws.luka.isa.forum.dto.RoleDTO;
import ws.luka.isa.forum.model.Role;

@Repository
public interface RoleRepository extends AbstractedRepository<RoleDTO, Role> {
}
