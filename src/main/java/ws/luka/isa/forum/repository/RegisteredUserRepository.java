package ws.luka.isa.forum.repository;

import org.springframework.stereotype.Repository;
import ws.luka.isa.forum.dto.RegisteredUserDTO;
import ws.luka.isa.forum.model.RegisteredUser;

import java.util.Optional;

@Repository
public interface RegisteredUserRepository extends AbstractedRepository<RegisteredUserDTO, RegisteredUser> {
    

    Optional<RegisteredUser> findByUsername(String username);

    Optional<RegisteredUser> findByEmail(String email);
}
