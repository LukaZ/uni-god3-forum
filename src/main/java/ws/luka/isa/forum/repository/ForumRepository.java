package ws.luka.isa.forum.repository;

import org.springframework.stereotype.Repository;
import ws.luka.isa.forum.dto.ForumDTO;
import ws.luka.isa.forum.model.Forum;

@Repository
public interface ForumRepository extends AbstractedRepository<ForumDTO, Forum> {
}
