package ws.luka.isa.forum.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ws.luka.isa.forum.dto.PostDTO;
import ws.luka.isa.forum.model.Post;

@Repository
public interface PostRepository extends AbstractedRepository<PostDTO, Post> {
}
