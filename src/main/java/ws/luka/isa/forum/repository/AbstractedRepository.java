package ws.luka.isa.forum.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ws.luka.isa.forum.dto.AbstractDTO;
import ws.luka.isa.forum.model.AbstractModel;

@Repository
public interface AbstractedRepository<D extends AbstractDTO, M extends AbstractModel<D>> extends PagingAndSortingRepository<M, Long> {
}
