package ws.luka.isa.forum.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;
import ws.luka.isa.forum.dto.ForumUserDTO;
import ws.luka.isa.forum.model.ForumUser;

import java.util.Optional;

@Repository
public interface ForumUserRepository extends AbstractedRepository<ForumUserDTO, ForumUser>   {
    Optional<ForumUser> findByForumUsername(String forumUsername);


    // TODO: FINISH
    //Optional<String[]> getForumUserRoles(String forumUsername);
}
