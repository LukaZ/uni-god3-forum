package ws.luka.isa.forum.exceptionhandler;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ws.luka.isa.forum.exceptionhandler.exception.EmailAlreadyInUseException;
import ws.luka.isa.forum.exceptionhandler.exception.UsernameAlreadyExistsException;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {UsernameAlreadyExistsException.class})
    protected ResponseEntity<Object> handleUsernameAlreadyExistsException( RuntimeException ex, WebRequest request ) {
        String bodyOfResponse = "Username already in use";
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.CONFLICT, request);
    }

    @ExceptionHandler(value = {EmailAlreadyInUseException.class})
    protected ResponseEntity<Object> handleEmailInUseException( RuntimeException ex, WebRequest request ) {
        String bodyOfResponse = "Email already in use";
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.CONFLICT, request);
    }

    // Called when we try to access a entity that doesn't exist; ie. delete registered user with id 2 but it does not
    //  exist
    @ExceptionHandler(value = {EmptyResultDataAccessException.class})
    protected ResponseEntity<Object> handleEmptyResultDataAccessException( RuntimeException ex, WebRequest request ) {
        String bodyOfResponse = "Entity does not exist";
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }


}
