package ws.luka.isa.forum.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ws.luka.isa.forum.model.ForumUser;

import java.util.ArrayList;

@Getter
@Setter
@NoArgsConstructor
public class ForumUserDTO extends AbstractDTO<ForumUser> {
    private String forumUsername;
    private RegisteredUserDTO registeredUser;
    private ArrayList<RoleDTO> roles = new ArrayList<>();

    public ForumUserDTO(Long id, String forumUsername, RegisteredUserDTO registeredUser, ArrayList<RoleDTO> roles) {
        super(id);
        this.forumUsername = forumUsername;
        this.registeredUser = registeredUser;
        this.roles = roles;
    }

}
