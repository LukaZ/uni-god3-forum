package ws.luka.isa.forum.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ws.luka.isa.forum.model.RegisteredUser;

import java.util.ArrayList;

@Getter
@Setter
@NoArgsConstructor
public class RegisteredUserDTO extends AbstractDTO<RegisteredUser> {
    private String username;
    private String password;
    private String email;
    private ArrayList<ForumUserDTO> forumUsers = new ArrayList<>();

    public RegisteredUserDTO(Long id, String username, String email, ArrayList<ForumUserDTO> forumUsers) {
        super(id);
        this.username = username;
        // this.password = password;
        this.email = email;
        this.forumUsers = forumUsers;
    }

    //    public RegisteredUserDTO(Long id, String username, String password, String email, ArrayList<ForumUserDTO> forumUsers){
//        super(id);
//        this.username = username;
//        this.password = password;
//        this.email = email;
//        this.forumUsers = forumUsers;
//    }

}
