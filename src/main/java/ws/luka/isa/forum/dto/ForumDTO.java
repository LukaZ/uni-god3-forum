package ws.luka.isa.forum.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ws.luka.isa.forum.model.Forum;

import java.util.ArrayList;

@Getter @Setter
@NoArgsConstructor
public class ForumDTO extends AbstractDTO<Forum>{
    private Boolean isPublic;
    private String name;
    private ArrayList<PostDTO> posts = new ArrayList<>();
    private ArrayList<ForumUserDTO> forumUsers = new ArrayList<>();

}
