package ws.luka.isa.forum.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ws.luka.isa.forum.model.Role;

import java.util.ArrayList;

@Getter
@Setter
@NoArgsConstructor
public class RoleDTO extends AbstractDTO<Role> {
    private String name;
    private ArrayList<ForumUserDTO> forumUsers = new ArrayList<>();

    public RoleDTO(Long id, String name, ArrayList<ForumUserDTO> forumUsers) {
        super(id);
        this.name = name;
        this.forumUsers = forumUsers;
    }

}
