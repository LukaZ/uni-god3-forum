package ws.luka.isa.forum.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
@NoArgsConstructor
public class AuthResponseDTO {
//    private String username;
    private String jwt;
//    private ArrayList<String> grantedAuthorities;


    public AuthResponseDTO(String jwt) {
        this.jwt = jwt;
    }
}
