package ws.luka.isa.forum.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ws.luka.isa.forum.model.File;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FileDTO extends AbstractDTO<File> {
    private String description;
    private String url;
    private PostDTO post;


}
