package ws.luka.isa.forum.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ws.luka.isa.forum.model.Topic;

import java.util.ArrayList;

@Getter
@Setter
@NoArgsConstructor
public class TopicDTO extends AbstractDTO<Topic>{
    private String topicName;
    private ArrayList<PostDTO> posts = new ArrayList<>();

    public TopicDTO(Long id, String topicName, ArrayList<PostDTO> posts) {
        super(id);
        this.topicName = topicName;
        this.posts = posts;
    }

}
