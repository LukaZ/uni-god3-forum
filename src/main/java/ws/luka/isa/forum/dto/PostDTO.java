package ws.luka.isa.forum.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ws.luka.isa.forum.model.Post;

import java.util.ArrayList;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class PostDTO extends AbstractDTO<Post> {
    private Date publicationDate;
    private String content;
    private TopicDTO topic;
    private ArrayList<FileDTO> files;

}
