package ws.luka.isa.forum.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ws.luka.isa.forum.model.AbstractModel;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractDTO<M extends AbstractModel> {
    protected Long id;
}
