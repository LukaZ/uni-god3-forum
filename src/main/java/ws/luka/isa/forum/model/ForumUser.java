package ws.luka.isa.forum.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import ws.luka.isa.forum.dto.ForumUserDTO;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
//@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class ForumUser extends AbstractModel<ForumUserDTO> {

//    @JsonBackReference(value = "registered_user-forum_user")
    @ManyToOne(optional = false)
    @JsonIgnoreProperties({"forumUsers"})
    private RegisteredUser registeredUser;

    //	Buduci da na osnovu dijagrama Registereduser moze da ima vise ForumUser naloga moramo ponuditi neki oblik
//	identifikacije UserDetails servisu, samim time dodajemo polje forumUsername
//	TODO Proveriti da li je adekvatno da koristimo istu lozinku kao sto i RegisteredUser koji je vezan za ForumUser ?
    private String forumUsername;

    //	Do not provide a mappedBy so that if a role is delete
//	the user wont be deleted as well ?
//    @JsonManagedReference(value = "forum_user-role")
    @ManyToMany
    @JsonIgnoreProperties({"forumUsers"})
    private Set<Role> roles = new HashSet<Role>();

//    @JsonBackReference(value = "forum-forum_user")
    @ManyToOne(optional = false)
    private Forum forum;

    @ManyToOne
    private ForumUser forumUser;

}
