package ws.luka.isa.forum.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ws.luka.isa.forum.dto.TopicDTO;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;


@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Topic extends AbstractModel<TopicDTO> {
	private String topicName;

	@ManyToOne
	@JsonIgnoreProperties({"topics", "forumUsers"})
	private Forum forum;

	@OneToMany(mappedBy = "topic", cascade = CascadeType.REMOVE)
	@JsonIgnoreProperties({"topic", "files"})
	private Set<Post> posts = new HashSet<Post>();
}

