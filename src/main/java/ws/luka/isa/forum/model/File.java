package ws.luka.isa.forum.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;
import ws.luka.isa.forum.dto.FileDTO;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;


@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class File extends AbstractModel<FileDTO> {
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String description;
    private String url;

    @ManyToOne
    @JsonIgnoreProperties("files")
    private Post post;

}
