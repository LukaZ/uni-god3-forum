package ws.luka.isa.forum.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ws.luka.isa.forum.dto.AbstractDTO;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@Data
@MappedSuperclass
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractModel<DTO extends AbstractDTO> implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

//    public abstract DTO convertToDTO();


    //TODO modularize conversion to DTO
//    public DTO convertToDTO(Class<DTO> dtoClass) {
//        ModelMapper modelMapper = new ModelMapper();
//        return modelMapper.map(this, dtoClass.class);
//    }
}
