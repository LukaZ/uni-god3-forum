package ws.luka.isa.forum.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ws.luka.isa.forum.dto.RoleDTO;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
//@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Role extends AbstractModel<RoleDTO> {
    @Column(nullable = false)
    private String name;

//    @JsonBackReference(value = "forum_user-role")
    @ManyToMany(mappedBy = "roles", cascade = CascadeType.ALL)
    private Set<ForumUser> forumUsers = new HashSet<ForumUser>();

}
