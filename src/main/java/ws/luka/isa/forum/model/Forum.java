package ws.luka.isa.forum.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ws.luka.isa.forum.dto.ForumDTO;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Forum extends AbstractModel<ForumDTO> {
	private String name;
	private Boolean isPublic;

	@OneToMany(mappedBy = "forum", cascade = CascadeType.REMOVE) // When the forum is deleted, delete associated data
	private Set<Topic> topics = new HashSet<Topic>();

	@OneToMany(mappedBy = "forumUser", cascade = CascadeType.REMOVE) // When the forum is deleted, delete associated data
	@JsonIgnoreProperties({"forumUsers"})
	private Set<ForumUser> forumUsers = new HashSet<ForumUser>();

}
