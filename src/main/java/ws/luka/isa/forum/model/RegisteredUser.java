package ws.luka.isa.forum.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ws.luka.isa.forum.dto.RegisteredUserDTO;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
//@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class RegisteredUser extends AbstractModel<RegisteredUserDTO> {
	@Column(nullable = false)
	private String username;

	@Column(nullable = false)
	private String password;

	@Column(nullable = false)
	private String email;


	//, "registeredUser", "forum"
	@OneToMany(mappedBy = "registeredUser", cascade = CascadeType.ALL)
	@JsonIgnoreProperties({"forumUsers", "forum", "registeredUser"})
	private Set<ForumUser> forumUsers = new HashSet<ForumUser>();

	public RegisteredUser(Long id, String username, String password, String email, Set<ForumUser> forumUsers) {
		super(id);
		this.username = username;
		this.password = password;
		this.email = email;
		this.forumUsers = forumUsers;
	}
}
