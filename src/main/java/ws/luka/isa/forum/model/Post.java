package ws.luka.isa.forum.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;
import ws.luka.isa.forum.dto.PostDTO;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
//@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Post extends AbstractModel<PostDTO> {
    @Temporal(TemporalType.TIMESTAMP)
    private Date publicationDate;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    private String content;

    @ManyToOne(optional = false)
    @JsonIgnoreProperties({"posts"})
    private Topic topic;

    // Orphan removal - remove dependent files in case the original post is deleted for storage optimization
    @OneToMany(mappedBy = "post", orphanRemoval = true)
    @JsonIgnoreProperties({"post"})
    private Set<File> files = new HashSet<File>();

}
