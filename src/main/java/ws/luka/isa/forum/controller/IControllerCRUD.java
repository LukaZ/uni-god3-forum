package ws.luka.isa.forum.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface IControllerCRUD<M> {
    Page<M> findAll(Pageable pageable);

    Optional<M> findOne(Long id);

    M save(M item);

    void delete(Long id);

    void delete(M item);
}

