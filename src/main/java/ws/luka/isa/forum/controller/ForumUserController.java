package ws.luka.isa.forum.controller;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import ws.luka.isa.forum.dto.ForumUserDTO;
import ws.luka.isa.forum.model.ForumUser;
import ws.luka.isa.forum.repository.ForumUserRepository;
import ws.luka.isa.forum.service.ForumUserService;

@Slf4j
@RestController
@RequestMapping("/api/forum-users")
@CrossOrigin(origins = "http://localhost:4200")
public class ForumUserController extends AbstractController<ForumUserDTO, ForumUser, ForumUserRepository, ForumUserService> {

    @Autowired
    private ForumUserService forumUserService;

    public ForumUserController(ForumUserService service) {
        super(service);
    }

    @PostMapping(value = "/xml")
    public ForumUser uploadFile(@RequestParam("file") MultipartFile file) {
        if (!file.isEmpty()) {
            try {
                XmlMapper xmlMapper = new XmlMapper();
                String xml = this.inputStreamToString(file.getInputStream());
                ForumUser value = xmlMapper.readValue(xml, ForumUser.class);
                return service.save(value);
            } catch (Exception e) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "You failed to upload because " + e.getMessage());
            }
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "You failed to upload because the file was empty.");
        }
    }
}
