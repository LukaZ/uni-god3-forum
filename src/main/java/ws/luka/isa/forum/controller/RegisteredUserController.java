package ws.luka.isa.forum.controller;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import ws.luka.isa.forum.dto.RegisteredUserDTO;
import ws.luka.isa.forum.model.RegisteredUser;
import ws.luka.isa.forum.repository.RegisteredUserRepository;
import ws.luka.isa.forum.service.RegisteredUserService;


@Slf4j
@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(path = "/api/registered-users")
public class RegisteredUserController extends AbstractController<RegisteredUserDTO, RegisteredUser, RegisteredUserRepository, RegisteredUserService> {
    @Autowired
    private RegisteredUserService registeredUserService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    Logger logger = LoggerFactory.getLogger(RegisteredUserController.class);


    public RegisteredUserController(RegisteredUserService service) {
        super(service);
    }

    @Override
    @PostMapping
    public RegisteredUser save(@RequestBody RegisteredUser item) {
        item.setPassword(passwordEncoder.encode(item.getPassword()));
        return super.save(item);
    }

    @Override
    @PutMapping
    public RegisteredUser update(@RequestBody RegisteredUser item) {
        item.setPassword(passwordEncoder.encode(item.getPassword()));
        return super.update(item);
    }

    @PostMapping(value = "/xml")
    public RegisteredUser uploadFile(@RequestParam("file") MultipartFile file) {
        if (!file.isEmpty()) {
            try {
                XmlMapper xmlMapper = new XmlMapper();
                String xml = this.inputStreamToString(file.getInputStream());
                RegisteredUser value = xmlMapper.readValue(xml, RegisteredUser.class);
                return service.save(value);
            } catch (Exception e) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "You failed to upload because " + e.getMessage());
            }
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "You failed to upload because the file was empty.");
        }
    }

}
