package ws.luka.isa.forum.controller;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import ws.luka.isa.forum.dto.PostDTO;
import ws.luka.isa.forum.model.Post;
import ws.luka.isa.forum.repository.PostRepository;
import ws.luka.isa.forum.service.PostService;

@Controller
@RequestMapping(path = "/api/posts")
@CrossOrigin(origins = "http://localhost:4200")
public class PostController extends AbstractController<PostDTO, Post, PostRepository, PostService> {
    @Autowired
    private PostService postService;

    public PostController(PostService service) {
        super(service);
    }

    @PostMapping(value = "/xml")
    public Post uploadFile(@RequestParam("file") MultipartFile file) {
        if (!file.isEmpty()) {
            try {
                XmlMapper xmlMapper = new XmlMapper();
                String xml = this.inputStreamToString(file.getInputStream());
                Post value = xmlMapper.readValue(xml, Post.class);
                return service.save(value);
            } catch (Exception e) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "You failed to upload because " + e.getMessage());
            }
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "You failed to upload because the file was empty.");
        }
    }
}
