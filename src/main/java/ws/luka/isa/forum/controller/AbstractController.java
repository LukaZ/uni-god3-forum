package ws.luka.isa.forum.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import ws.luka.isa.forum.dto.AbstractDTO;
import ws.luka.isa.forum.model.AbstractModel;
import ws.luka.isa.forum.repository.AbstractedRepository;
import ws.luka.isa.forum.service.AbstractService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Optional;

//Abstraction of a controller since all we do in controllers is CRUD;
//  if we need special functionality for one of the CRUD operations we can just @Override the method
@RestController
@CrossOrigin(origins = "http://localhost:4200")
public abstract class AbstractController<
        D extends AbstractDTO<M>,
        M extends AbstractModel<D>,
        R extends AbstractedRepository<D, M>,
        S extends AbstractService<D, M, R>> {

    @Autowired
    protected final S service;

    public AbstractController(S service) {
        this.service = service;
    }

    @GetMapping()
    public Page<M> findAll(Pageable pageable) {
        return service.findAll(pageable);
    }

    @GetMapping(path = "/{id}")
    public Optional<M> findOne(@PathVariable("id") Long id) {
        return service.findOne(id);
    }

    @PostMapping
    public M save(@RequestBody M item) {
        try {
            XmlMapper xmlMapper = new XmlMapper();
            String xml = xmlMapper.writeValueAsString(item);
            System.out.println(xml);
        } catch (JsonProcessingException ex) {
            System.out.println(ex.getMessage());
        }

        return service.save(item);
    }

    @PutMapping
    public M update(@RequestBody M item) {
        try {
            Optional<M> possibleItem = service.findOne(item.getId());
            if (possibleItem.isPresent()) {
                item.setId(possibleItem.get().getId());
                return service.save(item);
            }
        } catch (Exception exception) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, exception.getMessage(), exception);
        }
        return null;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        service.delete(id);
    }

    @DeleteMapping
    public void delete(@RequestBody M item) {
        service.delete(item);
    }

    protected String inputStreamToString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        String line;
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        br.close();
        return sb.toString();
    }
}
