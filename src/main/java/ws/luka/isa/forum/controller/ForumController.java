package ws.luka.isa.forum.controller;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import ws.luka.isa.forum.dto.ForumDTO;
import ws.luka.isa.forum.model.Forum;
import ws.luka.isa.forum.repository.ForumRepository;
import ws.luka.isa.forum.service.ForumService;

@RestController
@RequestMapping(path = "/api/forums")
@CrossOrigin(origins = "http://localhost:4200")
public class ForumController extends AbstractController<ForumDTO, Forum, ForumRepository, ForumService>{
    public ForumController(ForumService service) {
        super(service);
    }

    @PostMapping(value = "/xml")
    public Forum uploadFile(@RequestParam("file") MultipartFile file) {
        if (!file.isEmpty()) {
            try {
                XmlMapper xmlMapper = new XmlMapper();
                String xml = this.inputStreamToString(file.getInputStream());
                Forum value = xmlMapper.readValue(xml, Forum.class);
                return service.save(value);
            } catch (Exception e) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "You failed to upload because " + e.getMessage());
            }
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "You failed to upload because the file was empty.");
        }
    }
}
