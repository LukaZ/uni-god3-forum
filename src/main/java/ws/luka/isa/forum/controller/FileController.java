package ws.luka.isa.forum.controller;


import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import ws.luka.isa.forum.dto.FileDTO;
import ws.luka.isa.forum.model.File;
import ws.luka.isa.forum.repository.FileRepository;
import ws.luka.isa.forum.service.FileService;


@Slf4j // logging
@RestController
@RequestMapping("/api/files")
@CrossOrigin(origins = "http://localhost:4200")
public class FileController extends AbstractController<FileDTO, File, FileRepository, FileService> {
    @Autowired
    private FileService fileService;

    public FileController(FileService service) {
        super(service);
    }

    @PostMapping(value = "/xml")
    public File uploadFile(@RequestParam("file") MultipartFile file) {
        if (!file.isEmpty()) {
            try {
                XmlMapper xmlMapper = new XmlMapper();
                String xml = this.inputStreamToString(file.getInputStream());
                File value = xmlMapper.readValue(xml, File.class);
                return fileService.save(value);
            } catch (Exception e) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "You failed to upload because " + e.getMessage());
            }
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "You failed to upload because the file was empty.");
        }
    }
}
