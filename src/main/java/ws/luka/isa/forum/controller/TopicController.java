package ws.luka.isa.forum.controller;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import ws.luka.isa.forum.dto.TopicDTO;
import ws.luka.isa.forum.model.Topic;
import ws.luka.isa.forum.repository.TopicRepository;
import ws.luka.isa.forum.service.TopicService;


@RestController()
@RequestMapping(path = "/api/topics")
@CrossOrigin(origins = "http://localhost:4200")
public class TopicController extends AbstractController<TopicDTO, Topic, TopicRepository, TopicService>{

    @Autowired
    private TopicService topicService;

    public TopicController(TopicService service) {
        super(service);
    }

    @PostMapping(value = "/xml")
    public Topic uploadFile(@RequestParam("file") MultipartFile file) {
        if (!file.isEmpty()) {
            try {
                XmlMapper xmlMapper = new XmlMapper();
                String xml = this.inputStreamToString(file.getInputStream());
                Topic value = xmlMapper.readValue(xml, Topic.class);
                return service.save(value);
            } catch (Exception e) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "You failed to upload because " + e.getMessage());
            }
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "You failed to upload because the file was empty.");
        }
    }
}
