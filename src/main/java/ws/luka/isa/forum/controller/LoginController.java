package ws.luka.isa.forum.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ws.luka.isa.forum.dto.AuthRequestDTO;
import ws.luka.isa.forum.dto.AuthResponseDTO;
import ws.luka.isa.forum.dto.ForumUserDTO;
import ws.luka.isa.forum.dto.RegisteredUserDTO;
import ws.luka.isa.forum.exceptionhandler.exception.EmailAlreadyInUseException;
import ws.luka.isa.forum.exceptionhandler.exception.UsernameAlreadyExistsException;
import ws.luka.isa.forum.model.RegisteredUser;
import ws.luka.isa.forum.security.utils.TokenUtils;
import ws.luka.isa.forum.service.RegisteredUserService;

import java.util.ArrayList;

@Slf4j
@Controller
@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api")
public class LoginController {

    @Autowired
    private RegisteredUserService registeredUserService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private TokenUtils tokenUtils;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserDetailsService userDetailsService;

    @PostMapping(path = "/login")
    public ResponseEntity<AuthResponseDTO> login(@RequestBody AuthRequestDTO authRequest) throws Exception {
        try {
            // Generating login token, the token contains username and email
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
                    authRequest.getUsername(), authRequest.getPassword()
            );

            // Will thrown a AuthenticationException if it fails
            Authentication authentication = authenticationManager.authenticate(token);
            UserDetails userDetails = userDetailsService.loadUserByUsername(authRequest.getUsername());
            String generatedJWT = tokenUtils.generateToken(userDetails);

            AuthResponseDTO authResponseDTO = new AuthResponseDTO(generatedJWT);
            return new ResponseEntity<AuthResponseDTO>(authResponseDTO, HttpStatus.OK);

        } catch (UsernameNotFoundException usernameNotFoundException) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (AuthenticationException ae) {
            log.error(ae.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    @PostMapping(path = "/register")
    public ResponseEntity<RegisteredUserDTO> register(@RequestBody RegisteredUserDTO registeredUserDTO) throws Exception {
        if (registeredUserService.findByUsername(registeredUserDTO.getUsername()).isPresent()) {
            throw new UsernameAlreadyExistsException();
        }
        if (registeredUserService.findByEmail(registeredUserDTO.getEmail()).isPresent()) {
            throw new EmailAlreadyInUseException();
        }

        String encodedPassword = passwordEncoder.encode(registeredUserDTO.getPassword());
        RegisteredUser newRegisteredUser = new RegisteredUser(null, registeredUserDTO.getUsername(), encodedPassword,
                registeredUserDTO.getEmail(), null);

        registeredUserService.save(newRegisteredUser);

        // Manually creating a registered user so we don't send back the encoded password back
        RegisteredUserDTO registeredUserDTO1 = new RegisteredUserDTO();
        registeredUserDTO1.setId(newRegisteredUser.getId());
        registeredUserDTO1.setUsername(newRegisteredUser.getUsername());
        registeredUserDTO1.setPassword("");
        registeredUserDTO1.setEmail(newRegisteredUser.getEmail());
        registeredUserDTO1.setForumUsers(new ArrayList<ForumUserDTO>());
        return new ResponseEntity<RegisteredUserDTO>(registeredUserDTO1, HttpStatus.CREATED);
    }

}

