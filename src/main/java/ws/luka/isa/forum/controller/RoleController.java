package ws.luka.isa.forum.controller;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import ws.luka.isa.forum.dto.RoleDTO;
import ws.luka.isa.forum.model.Role;
import ws.luka.isa.forum.repository.RoleRepository;
import ws.luka.isa.forum.service.RoleService;

@RestController
@RequestMapping("/api/roles")
@CrossOrigin(origins = "http://localhost:4200")
public class RoleController extends AbstractController<RoleDTO, Role, RoleRepository, RoleService> {

    @Autowired
    private RoleService roleService;

    public RoleController(RoleService roleService) {
        super(roleService);
    }

    @PostMapping(value = "/xml")
    public Role uploadFile(@RequestParam("file") MultipartFile file) {
        if (!file.isEmpty()) {
            try {
                XmlMapper xmlMapper = new XmlMapper();
                String xml = this.inputStreamToString(file.getInputStream());
                Role value = xmlMapper.readValue(xml, Role.class);
                return service.save(value);
            } catch (Exception e) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "You failed to upload because " + e.getMessage());
            }
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "You failed to upload because the file was empty.");
        }
    }
}
