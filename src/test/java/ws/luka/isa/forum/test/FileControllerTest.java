package ws.luka.isa.forum.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ws.luka.isa.forum.controller.FileController;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class FileControllerTest {

    @Autowired
    private FileController controller;

    @org.junit.jupiter.api.Test
    void findAll() {
        assertNotNull(controller);

    }

    @org.junit.jupiter.api.Test
    void findOne() {
    }

    @org.junit.jupiter.api.Test
    void save() {
    }

    @org.junit.jupiter.api.Test
    void update() {
    }

    @org.junit.jupiter.api.Test
    void delete() {
    }

    @org.junit.jupiter.api.Test
    void testDelete() {
    }

    @org.junit.jupiter.api.Test
    void uploadFile() {
    }
}